# Pragozor CMS

This is Pragozor CMS. Content Manager & API for Pragozor Portal.

It is used project Strapi (https://github.com/strapi/strapi/blob/master/README.md).

Pragozor is a web application intended for the public, which can learn interesting numbers and analyzes about Prague.
It is part of the Golemio Data Dlatform, developed in Angular&TypeScript.

Developed by http://operatorict.cz

## Installation & run

All about installation & run this project is written here: https://github.com/strapi/strapi/blob/master/README.md.


## Troubleshooting

Contact roszkowski.wojciech@operatorict.cz
