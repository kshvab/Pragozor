module.exports = [
  "strapi::errors",
  {
      name: "strapi::security",
      config: {
          contentSecurityPolicy: {
              useDefaults: true,
              directives: {
                  "connect-src": ["'self'", "https:"],
                  "img-src": ["'self'", "data:", "blob:", "*.s3-prague.stor.geetoo.com"],
                  "media-src": ["'self'", "data:", "blob:"],
                  upgradeInsecureRequests: null,
              },
          },
      },
  },
  "strapi::cors",
  "strapi::poweredBy",
  "strapi::logger",
  "strapi::query",
  "strapi::body",
  "strapi::session",
  "strapi::favicon",
  "strapi::public",
];
