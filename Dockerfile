FROM node:14

WORKDIR /srv/app

ENV NODE_ENV production

COPY package.json yarn.lock ./
RUN yarn install && \
    yarn build
COPY . .

EXPOSE 1337

CMD ["npm", "start"]
